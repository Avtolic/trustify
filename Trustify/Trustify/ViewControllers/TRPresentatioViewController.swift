//
//  TRPresentatioViewController.swift
//  Trustify
//
//  Created by Yury on 20/09/15.
//  Copyright © 2015 Trustify. All rights reserved.
//

import UIKit
import SMPageControl

class TRPresentatioViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var pageControl: SMPageControl!
    @IBOutlet weak var slideScrollView: UIScrollView!
    @IBOutlet weak var slideText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        signInButton.layer.borderColor = UIColor(red: 218/255.0, green: 234/255.0, blue: 249/255.0, alpha: 1.0).CGColor
        signInButton.layer.borderWidth = 1
        pageControl.numberOfPages = 4
        pageControl.pageIndicatorTintColor = UIColor(red: 218/255.0, green: 234/255.0, blue: 249/255.0, alpha: 1.0)
        pageControl.currentPageIndicatorTintColor = UIColor(red: 111/255.0, green: 180/255.0, blue: 248/255.0, alpha: 1.0)
        pageControl.indicatorMargin = 15
        pageControl.indicatorDiameter = 18
    }

    let slideTexts = [
        "Feel like something\nis not quite right?",
        "At the push of a button, our investigators are at your service.",
        "They'll text or email you with answers, bringing much needed peace of mind.",
        "When you want to know the truth, quick answers are just a few clicks away."]
    
    @IBAction func pageChanged() {
        let xOffset = CGFloat(pageControl.currentPage) * slideScrollView.frame.size.width
        slideScrollView.setContentOffset(CGPointMake(xOffset, 0), animated: true)
        slideText.text = slideTexts[pageControl.currentPage]
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let slideNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width);
        pageControl.currentPage = Int(slideNumber)
        slideText.text = slideTexts[pageControl.currentPage]
    }
}
